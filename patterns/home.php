<?php

/*
 * Title: Site Home
 * Slug: somit-page/home
 * Categories: somit-pages
 * Viewport Width: 1500
 */
?>

<!-- wp:group {"tagName":"section","align":"full","backgroundColor":"brand","className":"is-style-horizontal-padded","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull is-style-horizontal-padded has-brand-background-color has-background" id="qui-som"><!-- wp:spacer {"height":"65px","className":"is-style-show-mobile-tablet"} -->
<div style="height:65px" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"6rem","className":"is-style-show-desktop"} -->
<div style="height:6rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:group {"className":"is-style-default is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group is-style-default is-style-show-desktop"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"}},"layout":{"type":"default"}} -->
<div class="wp-block-group"><!-- wp:heading {"level":1,"style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}}},"textColor":"typography"} -->
<h1 class="wp-block-heading has-typography-color has-text-color has-link-color">Desenvolupem solucions digitals amb una <mark style="background-color:rgba(0, 0, 0, 0)" class="has-inline-color has-main-color"><strong>visió estratègica</strong></mark></h1>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}},"typography":{"fontSize":"20px"}},"textColor":"typography"} -->
<p class="has-typography-color has-text-color has-link-color" style="font-size:20px">Ajudem a millorar els processos productius i el desenvolupament de les eines digitals de les cooperatives i les entitats de l’economia social.</p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"className":"is-style-hidden"} -->
<div class="wp-block-buttons is-style-hidden"><!-- wp:button {"style":{"typography":{"fontSize":"20px"}},"className":"is-style-fill"} -->
<div class="wp-block-button has-custom-font-size is-style-fill" style="font-size:20px"><a class="wp-block-button__link wp-element-button" href="#products">Més informació</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"}},"layout":{"type":"default"}} -->
<div class="wp-block-group"><!-- wp:image {"id":285,"sizeSlug":"full","linkDestination":"none","align":"center","className":"is-style-default"} -->
<figure class="wp-block-image aligncenter size-full is-style-default"><img src="https://somit.coop/wp-content/uploads/2024/01/mega-creator-01.png" alt="" class="wp-image-285"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"align":"wide","className":"is-style-show-mobile-tablet","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide is-style-show-mobile-tablet"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"}},"layout":{"type":"constrained"}} -->
<div class="wp-block-group"><!-- wp:image {"id":238,"sizeSlug":"full","linkDestination":"none","align":"center","className":"is-style-no-padded"} -->
<figure class="wp-block-image aligncenter size-full is-style-no-padded"><img src="https://somit.coop/wp-content/uploads/2024/01/isometric-computer-monitor-with-programming-code-on-screen-1main.png" alt="" class="wp-image-238"/></figure>
<!-- /wp:image -->

<!-- wp:spacer {"height":"2rem"} -->
<div style="height:2rem" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer --></div>
<!-- /wp:group -->

<!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"}},"layout":{"type":"default"}} -->
<div class="wp-block-group"><!-- wp:heading {"level":1,"style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}}},"textColor":"typography"} -->
<h1 class="wp-block-heading has-typography-color has-text-color has-link-color">Desenvolupem productes tecnològics amb una <mark style="background-color:rgba(0, 0, 0, 0)" class="has-inline-color has-main-color"><strong>visió estratègica</strong></mark></h1>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}}},"textColor":"typography"} -->
<p class="has-typography-color has-text-color has-link-color">Ajudem a millorar els processos productius i el desenvolupament de les eines digitals de les cooperatives i les entitats de l’economia social.</p>
<!-- /wp:paragraph -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"left"}} -->
<div class="wp-block-buttons"><!-- wp:button -->
<div class="wp-block-button"><a class="wp-block-button__link wp-element-button" href="#products">Més informació</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"7rem"} -->
<div style="height:7rem" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer --></section>
<!-- /wp:group -->

<!-- wp:group {"tagName":"section","align":"full","className":"is-style-horizontal-padded","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull is-style-horizontal-padded" id="mission"><!-- wp:spacer {"height":"6rem","className":"is-style-show-desktop"} -->
<div style="height:6rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"4rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:4rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"33.33%"}},"backgroundColor":"base","className":"is-style-no-padding","layout":{"type":"default"}} -->
<div class="wp-block-group is-style-no-padding has-base-background-color has-background"><!-- wp:heading {"level":3,"style":{"elements":{"link":{"color":{"text":"var:preset|color|main"}}},"typography":{"fontSize":"30px","fontStyle":"normal","fontWeight":"700"}},"textColor":"main"} -->
<h3 class="wp-block-heading has-main-color has-text-color has-link-color" style="font-size:30px;font-style:normal;font-weight:700"><mark style="background-color:rgba(0, 0, 0, 0)" class="has-inline-color has-main-color"><strong>1</strong> </mark>Visió</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Volem ser referents oferint solucions digitals innovadores basades en eines mancomunades i treball col·laboratiu.<br></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"50px"} -->
<div class="wp-block-column" style="flex-basis:50px"></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"33.33%"}},"className":"is-style-no-padding","layout":{"type":"default"}} -->
<div class="wp-block-group is-style-no-padding"><!-- wp:heading {"level":3,"style":{"typography":{"fontSize":"30px"}}} -->
<h3 class="wp-block-heading" style="font-size:30px"><mark style="background-color:rgba(0, 0, 0, 0)" class="has-inline-color has-main-color"><strong>2</strong></mark> Missió</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Oferim solucions digitals al servei de la transformació social.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"50px"} -->
<div class="wp-block-column" style="flex-basis:50px"></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"33.33%"}},"className":"is-style-no-padding","layout":{"type":"default"}} -->
<div class="wp-block-group is-style-no-padding"><!-- wp:heading {"level":3,"style":{"typography":{"fontSize":"30px"}}} -->
<h3 class="wp-block-heading" style="font-size:30px"><mark style="background-color:rgba(0, 0, 0, 0)" class="has-inline-color has-main-color"><strong>3</strong></mark> Estratègia</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Agrupem projectes cooperatius i impulsem un ecosistema IT que doni resposta als reptes de digitalització.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:spacer {"height":"7rem","className":"is-style-show-desktop"} -->
<div style="height:7rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"5rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:5rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer --></section>
<!-- /wp:group -->

<!-- wp:group {"tagName":"section","align":"full","backgroundColor":"main","className":"is-style-horizontal-padded","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull is-style-horizontal-padded has-main-background-color has-background" id="principles"><!-- wp:spacer {"height":"6rem","className":"is-style-show-desktop"} -->
<div style="height:6rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"4rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:4rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer -->

<!-- wp:group {"layout":{"type":"constrained","contentSize":"700px"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}},"typography":{"fontStyle":"normal","fontWeight":"400"}},"textColor":"base"} -->
<h2 class="wp-block-heading has-text-align-center has-base-color has-text-color has-link-color" style="font-style:normal;font-weight:400"><strong>Valors</strong> de Som IT Cooperatiu</h2>
<!-- /wp:heading --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"3rem"} -->
<div style="height:3rem" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:columns {"className":"is-style-col-gap-desktop"} -->
<div class="wp-block-columns is-style-col-gap-desktop"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:quote {"style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}},"typography":{"fontSize":"1.22rem"}},"backgroundColor":"base","textColor":"typography","className":"has-text-align-center h-48 is-style-default flex flex-col justify-center"} -->
<blockquote class="wp-block-quote has-text-align-center h-48 is-style-default flex flex-col justify-center has-typography-color has-base-background-color has-text-color has-background has-link-color" style="font-size:1.22rem"><!-- wp:heading {"level":3,"style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","className":"mb-0"} -->
<h3 class="wp-block-heading mb-0 has-secondary-color has-text-color has-link-color"><strong>Gestió democràtica i transparent</strong></h3>
<!-- /wp:heading --></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:quote {"style":{"typography":{"fontSize":"1.22rem"}},"backgroundColor":"base","className":"has-text-align-center h-48 is-style-default flex flex-col justify-center"} -->
<blockquote class="wp-block-quote has-text-align-center h-48 is-style-default flex flex-col justify-center has-base-background-color has-background" style="font-size:1.22rem"><!-- wp:heading {"level":3,"style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","className":"mb-0"} -->
<h3 class="wp-block-heading mb-0 has-secondary-color has-text-color has-link-color"><strong>Treball col·lectiu, obert i compartit</strong></h3>
<!-- /wp:heading --></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:quote {"style":{"layout":{"selfStretch":"fill","flexSize":null},"typography":{"fontSize":"1.22rem"}},"backgroundColor":"base","className":"has-text-align-center h-48 is-style-default flex flex-col justify-center"} -->
<blockquote class="wp-block-quote has-text-align-center h-48 is-style-default flex flex-col justify-center has-base-background-color has-background" style="font-size:1.22rem"><!-- wp:heading {"level":3,"style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","className":"mb-0"} -->
<h3 class="wp-block-heading mb-0 has-secondary-color has-text-color has-link-color"><strong>Diversitat</strong> i perspectiva de gènere</h3>
<!-- /wp:heading --></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"className":"is-style-col-gap-desktop"} -->
<div class="wp-block-columns is-style-col-gap-desktop"><!-- wp:column {"verticalAlignment":"top"} -->
<div class="wp-block-column is-vertically-aligned-top"><!-- wp:quote {"style":{"typography":{"fontSize":"1.22rem"}},"backgroundColor":"base","className":"has-text-align-center h-48 is-style-default flex flex-col justify-center"} -->
<blockquote class="wp-block-quote has-text-align-center h-48 is-style-default flex flex-col justify-center has-base-background-color has-background" style="font-size:1.22rem"><!-- wp:heading {"level":3,"style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","className":"mb-0"} -->
<h3 class="wp-block-heading mb-0 has-secondary-color has-text-color has-link-color"><strong>Tecnologia ètica</strong></h3>
<!-- /wp:heading --></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:quote {"style":{"typography":{"fontSize":"1.22rem"}},"backgroundColor":"base","className":"has-text-align-center h-48 is-style-default flex flex-col justify-center"} -->
<blockquote class="wp-block-quote has-text-align-center h-48 is-style-default flex flex-col justify-center has-base-background-color has-background" style="font-size:1.22rem"><!-- wp:heading {"level":3,"style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","className":"mb-0"} -->
<h3 class="wp-block-heading mb-0 has-secondary-color has-text-color has-link-color"><strong>Sostenibilitat</strong></h3>
<!-- /wp:heading --></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:quote {"style":{"layout":{"selfStretch":"fill","flexSize":null},"typography":{"fontSize":"1.22rem"}},"backgroundColor":"base","className":"has-text-align-center h-48 is-style-default flex flex-col justify-center"} -->
<blockquote class="wp-block-quote has-text-align-center h-48 is-style-default flex flex-col justify-center has-base-background-color has-background" style="font-size:1.22rem"><!-- wp:heading {"level":3,"style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary","className":"mb-0"} -->
<h3 class="wp-block-heading mb-0 has-secondary-color has-text-color has-link-color">Qualitat i <strong>eficiència</strong></h3>
<!-- /wp:heading --></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:spacer {"height":"7rem","className":"is-style-show-desktop"} -->
<div style="height:7rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"5rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:5rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer --></section>
<!-- /wp:group -->

<!-- wp:group {"tagName":"section","align":"full","className":"is-style-horizontal-padded","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull is-style-horizontal-padded" id="partners"><!-- wp:spacer {"height":"6rem","className":"is-style-show-desktop"} -->
<div style="height:6rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"4rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:4rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer -->

<!-- wp:group {"layout":{"type":"constrained","contentSize":"700px"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"400"}}} -->
<h2 class="wp-block-heading has-text-align-center" style="font-style:normal;font-weight:400">Entitats <strong>sòcies</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center","fontSize":"subtitle"} -->
<p class="has-text-align-center has-subtitle-font-size">Som una cooperativa de segon grau que treballa per les entitats de l’ESS.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"3rem"} -->
<div style="height:3rem" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:columns {"verticalAlignment":"center"} -->
<div class="wp-block-columns are-vertically-aligned-center"><!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:image {"lightbox":{"enabled":false},"id":47,"sizeSlug":"full","linkDestination":"custom","align":"right","className":"is-style-show-desktop"} -->
<figure class="wp-block-image alignright size-full is-style-show-desktop"><a href="https://somosconexion.coop/" target="_blank" rel="noreferrer noopener"><img src="https://somit.coop/wp-content/uploads/2023/11/som-connexio-2.png" alt="" class="wp-image-47"/></a></figure>
<!-- /wp:image -->

<!-- wp:image {"lightbox":{"enabled":false},"id":47,"sizeSlug":"full","linkDestination":"custom","align":"center","className":"is-style-show-mobile-tablet"} -->
<figure class="wp-block-image aligncenter size-full is-style-show-mobile-tablet"><a href="https://somosconexion.coop/" target="_blank" rel="noreferrer noopener"><img src="https://somit.coop/wp-content/uploads/2023/11/som-connexio-2.png" alt="" class="wp-image-47"/></a></figure>
<!-- /wp:image -->

<!-- wp:spacer {"height":"2rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:2rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:image {"lightbox":{"enabled":false},"id":41,"sizeSlug":"full","linkDestination":"custom","align":"center"} -->
<figure class="wp-block-image aligncenter size-full"><a href="https://www.sommobilitat.coop/" target="_blank" rel="noreferrer noopener"><img src="https://somit.coop/wp-content/uploads/2023/11/logo-1.png" alt="" class="wp-image-41"/></a></figure>
<!-- /wp:image -->

<!-- wp:spacer {"height":"2rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:2rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer --></div>
<!-- /wp:column -->

<!-- wp:column {"verticalAlignment":"center"} -->
<div class="wp-block-column is-vertically-aligned-center"><!-- wp:image {"lightbox":{"enabled":false},"id":659,"sizeSlug":"full","linkDestination":"custom","align":"left","className":"is-style-show-desktop"} -->
<figure class="wp-block-image alignleft size-full is-style-show-desktop"><a href="https://finanzaseticas.net/" target="_blank" rel="noopener"><img src="https://somit.coop/wp-content/uploads/2024/03/logo-fundacion-finanzas-eticas.jpg" alt="" class="wp-image-659"/></a></figure>
<!-- /wp:image -->

<!-- wp:image {"id":659,"sizeSlug":"full","linkDestination":"none","align":"center","className":"is-style-show-mobile-tablet is-style-default"} -->
<figure class="wp-block-image aligncenter size-full is-style-show-mobile-tablet is-style-default"><img src="https://somit.coop/wp-content/uploads/2024/03/logo-fundacion-finanzas-eticas.jpg" alt="" class="wp-image-659"/></figure>
<!-- /wp:image -->

<!-- wp:spacer {"height":"2rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:2rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:spacer {"height":"3rem"} -->
<div style="height:3rem" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"className":"is-style-hidden","layout":{"type":"flex","orientation":"vertical","justifyContent":"center"}} -->
<div class="wp-block-group is-style-hidden"><!-- wp:paragraph {"align":"center","style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}}},"textColor":"typography","fontSize":"large"} -->
<p class="has-text-align-center has-typography-color has-text-color has-link-color has-large-font-size"><strong>Uneix-te com a entitat sòcia!</strong></p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"0px","style":{"layout":{"flexSize":"1em","selfStretch":"fixed"}}} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"center"}} -->
<div class="wp-block-buttons"><!-- wp:button {"backgroundColor":"main","textColor":"base","style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"fontSize":"subtitle"} -->
<div class="wp-block-button has-custom-font-size has-subtitle-font-size"><a class="wp-block-button__link has-base-color has-main-background-color has-text-color has-background has-link-color wp-element-button" href="#contact">Unir-m'hi</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"3rem","className":"is-style-show-desktop"} -->
<div style="height:3rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer --></section>
<!-- /wp:group -->

<!-- wp:group {"tagName":"section","align":"full","backgroundColor":"brand","className":"is-style-horizontal-padded","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull is-style-horizontal-padded has-brand-background-color has-background" id="services"><!-- wp:spacer {"height":"6rem","className":"is-style-show-desktop"} -->
<div style="height:6rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"4rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:4rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer -->

<!-- wp:group {"className":"is-style-show-tablet-desktop","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group is-style-show-tablet-desktop"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"400"}}} -->
<h2 class="wp-block-heading" style="font-style:normal;font-weight:400">Els <strong>serveis</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"left","style":{"layout":{"selfStretch":"fixed","flexSize":"40%"}}} -->
<p class="has-text-align-left">Solucions digitals que milloren els processos de gestió de manera integral.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:group {"className":"is-style-show-mobile","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group is-style-show-mobile"><!-- wp:heading {"style":{"typography":{"fontStyle":"normal","fontWeight":"400"}}} -->
<h2 class="wp-block-heading" style="font-style:normal;font-weight:400">Els <strong>serveis</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"right","style":{"layout":{"selfStretch":"fixed","flexSize":"40%"}}} -->
<p class="has-text-align-right">Solucions digitals que milloren els processos de gestió de manera integral.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"6vw"} -->
<div style="height:6vw" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"className":"is-style-no-padding","layout":{"type":"default"}} -->
<div class="wp-block-group is-style-no-padding"><!-- wp:quote {"style":{"typography":{"fontSize":"1.22rem"}},"backgroundColor":"base","className":"is-style-default h-auto lg:h-60"} -->
<blockquote class="wp-block-quote is-style-default h-auto lg:h-60 has-base-background-color has-background" style="font-size:1.22rem"><!-- wp:heading {"level":3} -->
<h3 class="wp-block-heading">Consultoria estratègica</h3>
<!-- /wp:heading --><cite>Proporcionem un servei de consultoria en processos i tecnologia. Amb el nostre coneixement i experiència, col·laborem amb les vostres entitats per analitzar, millorar i optimitzar processos i sistemes tecnològics.</cite></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:group --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"100px"} -->
<div class="wp-block-column" style="flex-basis:100px"></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:group {"className":"is-style-no-padding","layout":{"type":"default"}} -->
<div class="wp-block-group is-style-no-padding"><!-- wp:quote {"style":{"typography":{"fontSize":"1.22rem"}},"backgroundColor":"base","className":"is-style-default h-auto lg:h-60"} -->
<blockquote class="wp-block-quote is-style-default h-auto lg:h-60 has-base-background-color has-background" style="font-size:1.22rem"><!-- wp:heading {"level":3} -->
<h3 class="wp-block-heading">Solucions digitals</h3>
<!-- /wp:heading --><cite>Oferim serveis de digitalització de processos interns, desenvolupament d'eines digitals i projectes tecnològics en intercooperació.<br></cite></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:group --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:spacer {"height":"7rem","className":"is-style-show-desktop"} -->
<div style="height:7rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"5rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:5rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer --></section>
<!-- /wp:group -->

<!-- wp:group {"tagName":"section","align":"full","className":"is-style-hidden","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull is-style-hidden" id="products"><!-- wp:spacer {"height":"6rem","className":"is-style-show-desktop"} -->
<div style="height:6rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"4rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:4rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer -->

<!-- wp:group {"layout":{"type":"constrained","contentSize":"700px"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center","style":{"typography":{"fontStyle":"normal","fontWeight":"400"}}} -->
<h2 class="wp-block-heading has-text-align-center" style="font-style:normal;font-weight:400">Els <strong>productes</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">Solucions innovadores tenint en compte l'intercooperació&nbsp;i l'experiència en el desenvolupament de productes mancomunats.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"3rem","className":"is-style-show-desktop"} -->
<div style="height:3rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:quote {"className":"is-style-secondary"} -->
<blockquote class="wp-block-quote is-style-secondary"><!-- wp:media-text {"mediaId":282,"mediaLink":"https://somit.coop/02/","mediaType":"image","mediaWidth":20} -->
<div class="wp-block-media-text is-stacked-on-mobile" style="grid-template-columns:20% auto"><figure class="wp-block-media-text__media"><img src="https://somit.coop/wp-content/uploads/2024/01/02-1024x1024.png" alt="" class="wp-image-282 size-full"/></figure><div class="wp-block-media-text__content"><!-- wp:heading {"level":3,"style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary"} -->
<h3 class="wp-block-heading has-secondary-color has-text-color has-link-color">Oficina Virtual</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Proporciona als usuaris una àrea privada des de la qual poden gestionar les seves activitats en una entitat. Mitjançant aquest producte, els usuaris tenen accés a funcionalitats com consulta de factures, serveis contractats, dades personals i consum, entre altres. És una eina que facilita la gestió eficient de les seves activitats amb comoditat i accessibilitat.</p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:media-text --></blockquote>
<!-- /wp:quote -->

<!-- wp:quote {"className":"is-style-secondary"} -->
<blockquote class="wp-block-quote is-style-secondary"><!-- wp:media-text {"mediaId":283,"mediaLink":"https://somit.coop/03/","mediaType":"image","mediaWidth":20} -->
<div class="wp-block-media-text is-stacked-on-mobile" style="grid-template-columns:20% auto"><figure class="wp-block-media-text__media"><img src="https://somit.coop/wp-content/uploads/2024/01/03-1024x1024.png" alt="" class="wp-image-283 size-full"/></figure><div class="wp-block-media-text__content"><!-- wp:heading {"level":3,"style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary"} -->
<h3 class="wp-block-heading has-secondary-color has-text-color has-link-color">Mapa Comunitari</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Com usuari podràs veure i explorar les comunitats que et rodegen, i expressar el teu interès en formar part d'alguna d'elles. És una eina que fomenta la connexió i la participació en comunitats, facilitant la trobada de persones amb interessos similars i la possibilitat d'implicar-te en projectes i activitats conjuntes.</p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:media-text --></blockquote>
<!-- /wp:quote -->

<!-- wp:quote {"className":"is-style-secondary"} -->
<blockquote class="wp-block-quote is-style-secondary"><!-- wp:media-text {"mediaId":284,"mediaLink":"https://somit.coop/04/","mediaType":"image","mediaWidth":20} -->
<div class="wp-block-media-text is-stacked-on-mobile" style="grid-template-columns:20% auto"><figure class="wp-block-media-text__media"><img src="https://somit.coop/wp-content/uploads/2024/01/04-1024x1024.png" alt="" class="wp-image-284 size-full"/></figure><div class="wp-block-media-text__content"><!-- wp:heading {"level":3,"style":{"elements":{"link":{"color":{"text":"var:preset|color|secondary"}}}},"textColor":"secondary"} -->
<h3 class="wp-block-heading has-secondary-color has-text-color has-link-color">Gestió Societària</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Gestiona altas i baixas de membres, les seves aportacions a capital social obligatori i voluntari, els préstecs concedits i els dividends distribuïts. Aquesta eina simplifica i automatitza els processos de gestió societària per una entitat cooperativa.</p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:media-text --></blockquote>
<!-- /wp:quote -->

<!-- wp:spacer {"height":"7rem","className":"is-style-show-desktop"} -->
<div style="height:7rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"5rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:5rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer --></section>
<!-- /wp:group -->

<!-- wp:group {"tagName":"section","align":"full","className":"is-style-horizontal-padded","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull is-style-horizontal-padded" id="projects"><!-- wp:spacer {"height":"6rem","className":"is-style-show-desktop"} -->
<div style="height:6rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"4rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:4rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:spacer {"height":"3rem","className":"is-style-show-desktop"} -->
<div style="height:3rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:heading -->
<h2 class="wp-block-heading"><strong>Projectes</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Intercooperem amb projectes per cocrear eines i coneixement al servei de la comunitat.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"100px"} -->
<div class="wp-block-column" style="flex-basis:100px"></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:quote {"backgroundColor":"base","className":"is-style-ce h-auto lg:h-80"} -->
<blockquote class="wp-block-quote is-style-ce h-auto lg:h-80 has-base-background-color has-background"><!-- wp:group {"className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"right"}} -->
<div class="wp-block-group is-style-show-desktop"><!-- wp:image {"lightbox":{"enabled":false},"id":46,"sizeSlug":"full","linkDestination":"custom","align":"right"} -->
<figure class="wp-block-image alignright size-full"><a href="https://somcomunitats.coop/" target="_blank" rel="noreferrer noopener"><img src="https://somit.coop/wp-content/uploads/2023/11/Som-Comunitats_Simple_Logo-1.png" alt="" class="wp-image-46"/></a></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"className":"is-style-no-padding is-style-show-mobile-tablet","layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group is-style-no-padding is-style-show-mobile-tablet"><!-- wp:image {"lightbox":{"enabled":false},"id":46,"sizeSlug":"full","linkDestination":"custom","align":"left"} -->
<figure class="wp-block-image alignleft size-full"><a href="https://somcomunitats.coop/" target="_blank" rel="noreferrer noopener"><img src="https://somit.coop/wp-content/uploads/2023/11/Som-Comunitats_Simple_Logo-1.png" alt="" class="wp-image-46"/></a></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"3rem","width":"0px","style":{"layout":[]}} -->
<div style="height:3rem;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:heading {"level":4,"style":{"elements":{"link":{"color":{"text":"var:preset|color|som-comunitats"}}}},"textColor":"som-comunitats"} -->
<h4 class="wp-block-heading has-som-comunitats-color has-text-color has-link-color">Som Comunitats</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Des de Som IT, col·laborem amb altres 6 entitats per oferir una solució integral de gestió de Comunitats Energètiques.</p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"3rem","width":"0px","style":{"layout":[]}} -->
<div style="height:3rem;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer --></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns -->
<div class="wp-block-columns"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:quote {"backgroundColor":"base","className":"is-style-sm  h-auto lg:h-80"} -->
<blockquote class="wp-block-quote is-style-sm  h-auto lg:h-80 has-base-background-color has-background"><!-- wp:group {"className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"right"}} -->
<div class="wp-block-group is-style-show-desktop"><!-- wp:image {"lightbox":{"enabled":false},"id":41,"width":"auto","height":"75px","sizeSlug":"full","linkDestination":"custom","align":"right"} -->
<figure class="wp-block-image alignright size-full is-resized"><img src="https://somit.coop/wp-content/uploads/2023/11/logo-1.png" alt="" class="wp-image-41" style="width:auto;height:75px"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"className":"is-style-no-padding is-style-show-mobile-tablet","layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group is-style-no-padding is-style-show-mobile-tablet"><!-- wp:image {"lightbox":{"enabled":false},"id":41,"width":"auto","height":"75px","sizeSlug":"full","linkDestination":"custom","align":"left"} -->
<figure class="wp-block-image alignleft size-full is-resized"><img src="https://somit.coop/wp-content/uploads/2023/11/logo-1.png" alt="" class="wp-image-41" style="width:auto;height:75px"/></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"2rem"} -->
<div style="height:2rem" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:heading {"level":4,"style":{"elements":{"link":{"color":{"text":"var:preset|color|moves"}}}},"textColor":"moves"} -->
<h4 class="wp-block-heading has-moves-color has-text-color has-link-color">Sistemes IT Som Mobilitat</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Som IT coordina les activitats tecnològiques de Som Mobilitat amb l'objectiu de millorar les eines, processos i automatitzacions de l'ecosistema tecnològic.</p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column -->

<!-- wp:column {"width":"100px"} -->
<div class="wp-block-column" style="flex-basis:100px"></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:quote {"backgroundColor":"base","className":"is-style-sc  h-auto lg:h-80"} -->
<blockquote class="wp-block-quote is-style-sc  h-auto lg:h-80 has-base-background-color has-background"><!-- wp:group {"className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"right"}} -->
<div class="wp-block-group is-style-show-desktop"><!-- wp:image {"lightbox":{"enabled":false},"id":47,"width":"auto","height":"70px","sizeSlug":"full","linkDestination":"custom","align":"right"} -->
<figure class="wp-block-image alignright size-full is-resized"><a href="https://somosconexion.coop/" target="_blank" rel="noreferrer noopener"><img src="https://somit.coop/wp-content/uploads/2023/11/som-connexio-2.png" alt="" class="wp-image-47" style="width:auto;height:70px"/></a></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:group {"className":"is-style-no-padding is-style-show-mobile-tablet","layout":{"type":"flex","flexWrap":"nowrap"}} -->
<div class="wp-block-group is-style-no-padding is-style-show-mobile-tablet"><!-- wp:image {"lightbox":{"enabled":false},"id":47,"width":"auto","height":"65px","sizeSlug":"full","linkDestination":"custom","align":"left"} -->
<figure class="wp-block-image alignleft size-full is-resized"><a href="https://somosconexion.coop/" target="_blank" rel="noreferrer noopener"><img src="https://somit.coop/wp-content/uploads/2023/11/som-connexio-2.png" alt="" class="wp-image-47" style="width:auto;height:65px"/></a></figure>
<!-- /wp:image --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"2rem"} -->
<div style="height:2rem" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:heading {"level":4,"style":{"elements":{"link":{"color":{"text":"var:preset|color|som-connexio"}}}},"textColor":"som-connexio"} -->
<h4 class="wp-block-heading has-som-connexio-color has-text-color has-link-color">I+D Som Connexió</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Participem del projecte&nbsp;de millora de processos&nbsp;ERP, innovació amb IA amb perspectiva&nbsp;ètica&nbsp;i consultoria estratègica amb fundacions&nbsp;d'inserció&nbsp;laboral.<mark style="background-color:rgba(0, 0, 0, 0)" class="has-inline-color has-base-color">.</mark></p>
<!-- /wp:paragraph --></blockquote>
<!-- /wp:quote --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:spacer {"height":"7rem","className":"is-style-show-desktop"} -->
<div style="height:7rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"5rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:5rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer --></section>
<!-- /wp:group -->

<!-- wp:group {"tagName":"section","align":"full","backgroundColor":"brand","className":"is-style-horizontal-padded","layout":{"type":"constrained"}} -->
<section class="wp-block-group alignfull is-style-horizontal-padded has-brand-background-color has-background" id="team"><!-- wp:spacer {"height":"6rem","className":"is-style-show-desktop"} -->
<div style="height:6rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"4rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:4rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer -->

<!-- wp:group {"layout":{"type":"constrained","contentSize":"700px"}} -->
<div class="wp-block-group"><!-- wp:heading {"textAlign":"center"} -->
<h2 class="wp-block-heading has-text-align-center"><strong>Equip</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">Treballem per la cura i el desenvolupament dels membres de la nostra cooperativa.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"3rem","className":"is-style-show-desktop"} -->
<div style="height:3rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:group {"className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"top"}} -->
<div class="wp-block-group is-style-show-desktop"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"layout":{"type":"default"}} -->
<div class="wp-block-group"><!-- wp:spacer {"height":"1rem"} -->
<div style="height:1rem" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Equip tècnic</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Tenim un equip multidisciplinari&nbsp;amb una&nbsp;àmplia&nbsp;experiència&nbsp;per afrontar amb garanties projectes i serveis.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/IMG_8708-1.jpg","id":613,"dimRatio":30,"customOverlayColor":"#8a5a4a","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote is-style-secondary align-content-bottom","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote is-style-secondary align-content-bottom"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#8a5a4a"></span><img class="wp-block-cover__image-background wp-image-613" alt="" src="https://somit.coop/wp-content/uploads/2024/03/IMG_8708-1.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong>Enrico Stano</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Coordinador</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/IMG_8725-1.jpg","id":607,"dimRatio":30,"customOverlayColor":"#836e5c","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote is-style-secondary align-content-bottom","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote is-style-secondary align-content-bottom"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#836e5c"></span><img class="wp-block-cover__image-background wp-image-607" alt="" src="https://somit.coop/wp-content/uploads/2024/03/IMG_8725-1.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"left","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-left has-medium-font-size"><strong>Joan Caballero</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Coordinador</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/01/Daniil-Digtyar-1.png","id":388,"dimRatio":30,"customOverlayColor":"#817957","isUserOverlayColor":true,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote is-style-secondary align-content-bottom","layout":{"type":"default"}} -->
<div class="wp-block-cover wp-block-quote is-style-secondary align-content-bottom"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#817957"></span><img class="wp-block-cover__image-background wp-image-388" alt="" src="https://somit.coop/wp-content/uploads/2024/01/Daniil-Digtyar-1.png" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong>Daniil Digtyar</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Desenvolupador Odoo</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->

<!-- wp:group {"className":"is-style-show-mobile-tablet","layout":{"type":"flex","orientation":"vertical","justifyContent":"stretch"}} -->
<div class="wp-block-group is-style-show-mobile-tablet"><!-- wp:group {"style":{"layout":{"selfStretch":"fit","flexSize":null}},"layout":{"type":"default"}} -->
<div class="wp-block-group"><!-- wp:spacer {"height":"1rem"} -->
<div style="height:1rem" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:heading {"textAlign":"center","level":4} -->
<h4 class="wp-block-heading has-text-align-center">Equip tècnic</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">Tenim un equip&nbsp;multidisciplinari&nbsp;amb una&nbsp;àmplia&nbsp;experiència&nbsp;per afrontar amb garanties projectes i serveis.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/IMG_8708-1.jpg","id":613,"dimRatio":30,"customOverlayColor":"#8a5a4a","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote align-content-bottom is-style-secondary","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote align-content-bottom is-style-secondary"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#8a5a4a"></span><img class="wp-block-cover__image-background wp-image-613" alt="" src="https://somit.coop/wp-content/uploads/2024/03/IMG_8708-1.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong><strong>Enrico Stano</strong></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Coordinador</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/IMG_8725-1.jpg","id":607,"dimRatio":30,"customOverlayColor":"#836e5c","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote align-content-bottom is-style-secondary","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote align-content-bottom is-style-secondary"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#836e5c"></span><img class="wp-block-cover__image-background wp-image-607" alt="" src="https://somit.coop/wp-content/uploads/2024/03/IMG_8725-1.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"left","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-left has-medium-font-size"><strong><strong>Joan Caballero</strong></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Coordinador</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/01/Daniil-Digtyar-1.png","id":388,"dimRatio":30,"customOverlayColor":"#817957","isUserOverlayColor":true,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote align-content-bottom is-style-secondary","layout":{"type":"default"}} -->
<div class="wp-block-cover wp-block-quote align-content-bottom is-style-secondary"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#817957"></span><img class="wp-block-cover__image-background wp-image-388" alt="" src="https://somit.coop/wp-content/uploads/2024/01/Daniil-Digtyar-1.png" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong><strong>Daniil Digtyar</strong></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Desenvolupador Odoo</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"3rem","className":"is-style-show-desktop"} -->
<div style="height:3rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"30px","className":"is-style-show-mobile-tablet"} -->
<div style="height:30px" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer -->

<!-- wp:group {"className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"top"}} -->
<div class="wp-block-group is-style-show-desktop"><!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/foto_equip_javi_rp.jpg","id":809,"dimRatio":30,"customOverlayColor":"#7f6c4b","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote is-style-secondary align-content-bottom","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote is-style-secondary align-content-bottom"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#7f6c4b"></span><img class="wp-block-cover__image-background wp-image-809" alt="" src="https://somit.coop/wp-content/uploads/2024/03/foto_equip_javi_rp.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"left","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-left has-medium-font-size"><strong>Javier Carrillo</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Product Owner</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/IMG_8709-1.jpg","id":604,"dimRatio":30,"customOverlayColor":"#70613c","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote is-style-secondary align-content-bottom","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote is-style-secondary align-content-bottom"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#70613c"></span><img class="wp-block-cover__image-background wp-image-604" alt="" src="https://somit.coop/wp-content/uploads/2024/03/IMG_8709-1.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong>Guillem Alborch</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Consultor funcional</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/IMG_8712-1.jpg","id":610,"dimRatio":30,"customOverlayColor":"#846a49","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote is-style-secondary align-content-bottom","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote is-style-secondary align-content-bottom"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#846a49"></span><img class="wp-block-cover__image-background wp-image-610" alt="" src="https://somit.coop/wp-content/uploads/2024/03/IMG_8712-1.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"left","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-left has-medium-font-size"><strong>José Robles</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Desenvolupador Odoo</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/IMG_8718-1.jpg","id":598,"dimRatio":30,"customOverlayColor":"#775a36","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote is-style-secondary align-content-bottom","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote is-style-secondary align-content-bottom"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#775a36"></span><img class="wp-block-cover__image-background wp-image-598" alt="" src="https://somit.coop/wp-content/uploads/2024/03/IMG_8718-1.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong>Álvaro García</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Desenvolupador Odoo</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->

<!-- wp:group {"className":"is-style-show-mobile-tablet","layout":{"type":"flex","orientation":"vertical","justifyContent":"stretch"}} -->
<div class="wp-block-group is-style-show-mobile-tablet"><!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/foto_equip_javi_rp.jpg","id":809,"dimRatio":30,"customOverlayColor":"#7f6c4b","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote align-content-bottom is-style-secondary","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote align-content-bottom is-style-secondary"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#7f6c4b"></span><img class="wp-block-cover__image-background wp-image-809" alt="" src="https://somit.coop/wp-content/uploads/2024/03/foto_equip_javi_rp.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"left","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-left has-medium-font-size"><strong><strong>Javier Carrillo</strong></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Product Owner</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/IMG_8709-1.jpg","id":604,"dimRatio":30,"customOverlayColor":"#70613c","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote align-content-bottom is-style-secondary","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote align-content-bottom is-style-secondary"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#70613c"></span><img class="wp-block-cover__image-background wp-image-604" alt="" src="https://somit.coop/wp-content/uploads/2024/03/IMG_8709-1.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong><strong>Guillem Alborch</strong></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Consultor funcional</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/IMG_8712-1.jpg","id":610,"dimRatio":30,"customOverlayColor":"#846a49","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote align-content-bottom is-style-secondary","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote align-content-bottom is-style-secondary"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#846a49"></span><img class="wp-block-cover__image-background wp-image-610" alt="" src="https://somit.coop/wp-content/uploads/2024/03/IMG_8712-1.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"left","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-left has-medium-font-size"><strong><strong>José Robles</strong></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Desenvolupador Odoo</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/03/IMG_8718-1.jpg","id":598,"dimRatio":30,"customOverlayColor":"#775a36","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote align-content-bottom is-style-secondary","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote align-content-bottom is-style-secondary"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#775a36"></span><img class="wp-block-cover__image-background wp-image-598" alt="" src="https://somit.coop/wp-content/uploads/2024/03/IMG_8718-1.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group has-base-color has-text-color has-link-color"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong><strong>Álvaro García</strong></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Desenvolupador Odoo</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"3rem","className":"is-style-show-desktop"} -->
<div style="height:3rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"30px","className":"is-style-show-mobile-tablet"} -->
<div style="height:30px" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer -->

<!-- wp:group {"align":"full","className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"center","justifyContent":"center"}} -->
<div class="wp-block-group alignfull is-style-show-desktop"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"960px"}},"layout":{"type":"flex","orientation":"vertical","verticalAlignment":"center","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}}},"textColor":"typography","fontSize":"large"} -->
<p class="has-text-align-center has-typography-color has-text-color has-link-color has-large-font-size"><strong>Treballa amb nosaltres</strong></p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"0px","style":{"layout":{"flexSize":"1em","selfStretch":"fixed"}}} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"backgroundColor":"main","textColor":"base","style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}}} -->
<div class="wp-block-button"><a class="wp-block-button__link has-base-color has-main-background-color has-text-color has-background has-link-color wp-element-button" href="#contact">Més informació</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons -->

<!-- wp:spacer {"height":"9rem","width":"0px","className":"is-style-show-desktop","style":{"layout":{"selfStretch":"fixed","flexSize":"3rem"}}} -->
<div style="height:9rem;width:0px" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"className":"is-style-show-mobile-tablet","layout":{"type":"flex","orientation":"vertical","justifyContent":"stretch"}} -->
<div class="wp-block-group is-style-show-mobile-tablet"><!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"3rem","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"layout":{"selfStretch":"fit","flexSize":null}},"layout":{"type":"flex","orientation":"vertical","verticalAlignment":"center","justifyContent":"center"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","fontSize":"large"} -->
<p class="has-text-align-center has-large-font-size"><strong>Treballa amb nosaltres</strong></p>
<!-- /wp:paragraph -->

<!-- wp:spacer {"height":"0px","style":{"layout":{"flexSize":"1em","selfStretch":"fixed"}}} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:buttons -->
<div class="wp-block-buttons"><!-- wp:button {"backgroundColor":"main","textColor":"base","style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}}} -->
<div class="wp-block-button"><a class="wp-block-button__link has-base-color has-main-background-color has-text-color has-background has-link-color wp-element-button" href="#contact">Més informació</a></div>
<!-- /wp:button --></div>
<!-- /wp:buttons -->

<!-- wp:spacer {"height":"0px","className":"is-style-show-desktop","style":{"layout":{"selfStretch":"fixed","flexSize":"3rem"}}} -->
<div style="height:0px" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer --></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:group {"className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"top"}} -->
<div class="wp-block-group is-style-show-desktop"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"layout":{"type":"default"}} -->
<div class="wp-block-group"><!-- wp:spacer {"height":"1rem"} -->
<div style="height:1rem" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:heading {"level":4} -->
<h4 class="wp-block-heading">Consell rector</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Les&nbsp;entitats sòcies&nbsp;participen en la presa de&nbsp;decisions&nbsp;de la cooperativa a través dels seus representants en el consell rector.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/01/arnau.jpg","id":382,"dimRatio":30,"customOverlayColor":"#494642","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"className":"wp-block-quote is-style-secondary align-content-bottom","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote is-style-secondary align-content-bottom"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#494642"></span><img class="wp-block-cover__image-background wp-image-382" alt="" src="https://somit.coop/wp-content/uploads/2024/01/arnau.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"left","placeholder":"Write title…","style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","fontSize":"medium"} -->
<p class="has-text-align-left has-base-color has-text-color has-link-color has-medium-font-size"><strong>Arnau Vilardell</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","fontSize":"x-small"} -->
<p class="has-base-color has-text-color has-link-color has-x-small-font-size">President / Som Mobilitat</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/01/bernat_team.jpg","id":383,"dimRatio":30,"customOverlayColor":"#8b827e","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"},"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"wp-block-quote is-style-secondary align-content-bottom","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote is-style-secondary align-content-bottom has-base-color has-text-color has-link-color"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#8b827e"></span><img class="wp-block-cover__image-background wp-image-383" alt="" src="https://somit.coop/wp-content/uploads/2024/01/bernat_team.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong>Bernat Alcolea</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Secretari / Som Connexió</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"}},"layout":{"type":"default"}} -->
<div class="wp-block-group"></div>
<!-- /wp:group --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"3rem","className":"is-style-show-desktop"} -->
<div style="height:3rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:group {"className":"is-style-show-mobile-tablet","layout":{"type":"flex","orientation":"vertical","justifyContent":"stretch"}} -->
<div class="wp-block-group is-style-show-mobile-tablet"><!-- wp:group {"style":{"layout":{"selfStretch":"fit","flexSize":null}},"layout":{"type":"default"}} -->
<div class="wp-block-group"><!-- wp:spacer {"height":"var:preset|spacing|70"} -->
<div style="height:var(--wp--preset--spacing--70)" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:heading {"textAlign":"center","level":4} -->
<h4 class="wp-block-heading has-text-align-center">Consell rector</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"align":"center"} -->
<p class="has-text-align-center">Les&nbsp;entitats sòcies&nbsp;participen en la presa de&nbsp;decisions&nbsp;de la cooperativa a través dels seus representants en el consell rector.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/01/arnau.jpg","id":382,"dimRatio":30,"customOverlayColor":"#494642","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"},"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"wp-block-quote align-content-bottom is-style-secondary","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote align-content-bottom is-style-secondary has-base-color has-text-color has-link-color"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#494642"></span><img class="wp-block-cover__image-background wp-image-382" alt="" src="https://somit.coop/wp-content/uploads/2024/01/arnau.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"left","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-left has-medium-font-size"><strong><strong>Arnau Vilardell</strong></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">President / Som Mobilitat</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->

<!-- wp:cover {"url":"https://somit.coop/wp-content/uploads/2024/01/bernat_team.jpg","id":383,"dimRatio":30,"customOverlayColor":"#8b827e","isUserOverlayColor":true,"isDark":false,"style":{"layout":{"selfStretch":"fixed","flexSize":"300px"},"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"wp-block-quote align-content-bottom is-style-secondary","layout":{"type":"default"}} -->
<div class="wp-block-cover is-light wp-block-quote align-content-bottom is-style-secondary has-base-color has-text-color has-link-color"><span aria-hidden="true" class="wp-block-cover__background has-background-dim-30 has-background-dim" style="background-color:#8b827e"></span><img class="wp-block-cover__image-background wp-image-383" alt="" src="https://somit.coop/wp-content/uploads/2024/01/bernat_team.jpg" data-object-fit="cover"/><div class="wp-block-cover__inner-container"><!-- wp:group {"layout":{"type":"flex","orientation":"vertical"}} -->
<div class="wp-block-group"><!-- wp:paragraph {"align":"center","placeholder":"Write title…","fontSize":"medium"} -->
<p class="has-text-align-center has-medium-font-size"><strong>Bernat Alcolea</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"fontSize":"x-small"} -->
<p class="has-x-small-font-size">Secretari / Som Connexió</p>
<!-- /wp:paragraph --></div>
<!-- /wp:group --></div></div>
<!-- /wp:cover -->

<!-- wp:spacer {"width":"0px","style":{"layout":{"flexSize":"30px","selfStretch":"fixed"}}} -->
<div style="height:100px;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer --></div>
<!-- /wp:group -->

<!-- wp:spacer {"height":"3rem","className":"is-style-show-desktop"} -->
<div style="height:3rem" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
<!-- /wp:spacer -->

<!-- wp:spacer {"height":"5rem","className":"is-style-show-mobile-tablet"} -->
<div style="height:5rem" aria-hidden="true" class="wp-block-spacer is-style-show-mobile-tablet"></div>
<!-- /wp:spacer --></section>
<!-- /wp:group -->
