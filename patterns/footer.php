<?php

/**
 * Title: SomIT Footer.
 * Slug: somit-pattern/footer
 * Categories: somit-pattern
 * Viewport Width: 1350
 */

?>

<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|typography"}}},"spacing":{"margin":{"top":"0","bottom":"0"}}},"backgroundColor":"brand","textColor":"base","className":"is-style-horizontal-padded","layout":{"type":"default"}} -->
<div class="wp-block-group alignfull is-style-horizontal-padded has-base-color has-brand-background-color has-text-color has-background has-link-color" id="contact" style="margin-top:0;margin-bottom:0"><!-- wp:spacer {"height":"6rem"} -->
  <div style="height:6rem" aria-hidden="true" class="wp-block-spacer"></div>
  <!-- /wp:spacer -->

  <!-- wp:group {"layout":{"type":"constrained"}} -->
  <div class="wp-block-group"><!-- wp:columns -->
    <div class="wp-block-columns"><!-- wp:column -->
      <div class="wp-block-column"><!-- wp:group {"className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"top"}} -->
        <div class="wp-block-group is-style-show-desktop"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"}},"layout":{"type":"default"}} -->
          <div class="wp-block-group"><!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|main"}}}},"textColor":"main","fontSize":"medium"} -->
            <p class="has-main-color has-text-color has-link-color has-medium-font-size"><strong>Dades de l'entitat</strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:paragraph -->
            <p>Som IT Cooperatiu SCCL<br>C/ Toló 18, 3<br>08301 Mataró (Barcelona)<br>España</p>
            <!-- /wp:paragraph -->

            <!-- wp:spacer {"height":"1em","width":"0px","style":{"layout":{}}} -->
            <div style="height:1em;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
            <!-- /wp:spacer -->
          </div>
          <!-- /wp:group -->

          <!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"}},"layout":{"type":"default"}} -->
          <div class="wp-block-group"><!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|main"}}}},"textColor":"main","fontSize":"medium"} -->
            <p class="has-main-color has-text-color has-link-color has-medium-font-size"><strong>Enllaços</strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:list {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"is-style-no-disc"} -->
            <ul class="is-style-no-disc has-base-color has-text-color has-link-color"><!-- wp:list-item -->
              <li><a href="/#qui-som">Som It Cooperatiu</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="#services">Serveis</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="#products">Productes</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="#projects">Projectes</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="#team">Equip</a></li>
              <!-- /wp:list-item -->
            </ul>
            <!-- /wp:list -->
          </div>
          <!-- /wp:group -->
        </div>
        <!-- /wp:group -->

        <!-- wp:group {"className":"is-style-show-mobile-tablet","layout":{"type":"flex","orientation":"vertical"}} -->
        <div class="wp-block-group is-style-show-mobile-tablet"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"}},"layout":{"type":"default"}} -->
          <div class="wp-block-group"><!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|main"}}}},"textColor":"main","fontSize":"medium"} -->
            <p class="has-main-color has-text-color has-link-color has-medium-font-size"><strong>Dades de l'entitat</strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:paragraph -->
            <p>Som IT Cooperatiu SCCL<br>C/ Toló 18, 3<br>08301 Mataró (Barcelona)<br>España</p>
            <!-- /wp:paragraph -->
          </div>
          <!-- /wp:group -->

          <!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"}},"layout":{"type":"default"}} -->
          <div class="wp-block-group"><!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|main"}}}},"textColor":"main","fontSize":"medium"} -->
            <p class="has-main-color has-text-color has-link-color has-medium-font-size"><strong>Enllaços</strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:list {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"is-style-no-disc"} -->
            <ul class="is-style-no-disc has-base-color has-text-color has-link-color"><!-- wp:list-item -->
              <li><a href="/#qui-som">Som It Cooperatiu</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="#services">Serveis</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="#products">Productes</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="#projects">Projectes</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="#team">Equip</a></li>
              <!-- /wp:list-item -->
            </ul>
            <!-- /wp:list -->

            <!-- wp:spacer {"height":"1em","width":"0px","style":{"layout":{}}} -->
            <div style="height:1em;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
            <!-- /wp:spacer -->
          </div>
          <!-- /wp:group -->
        </div>
        <!-- /wp:group -->

        <!-- wp:spacer {"height":"2em","className":"is-style-show-desktop"} -->
        <div style="height:2em" aria-hidden="true" class="wp-block-spacer is-style-show-desktop"></div>
        <!-- /wp:spacer -->

        <!-- wp:group {"className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","verticalAlignment":"top"}} -->
        <div class="wp-block-group is-style-show-desktop"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"}},"className":"is-style-default","layout":{"type":"default"}} -->
          <div class="wp-block-group is-style-default"><!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|main"}}}},"textColor":"main","fontSize":"medium"} -->
            <p class="has-main-color has-text-color has-link-color has-medium-font-size"><strong>Xarxes socials</strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:list {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"is-style-no-disc"} -->
            <ul class="is-style-no-disc has-base-color has-text-color has-link-color"><!-- wp:list-item -->
              <li><a href="https://www.linkedin.com/">Linkedin</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="https://twitter.com/">Twitter</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="https://mastodon.social">Mastodon</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="https://github.com">Github</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="https://gitlab.com">Gitlab</a></li>
              <!-- /wp:list-item -->
            </ul>
            <!-- /wp:list -->
          </div>
          <!-- /wp:group -->

          <!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"},"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"is-style-default","layout":{"type":"default"}} -->
          <div class="wp-block-group is-style-default has-base-color has-text-color has-link-color"><!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|main"}}}},"textColor":"main","fontSize":"medium"} -->
            <p class="has-main-color has-text-color has-link-color has-medium-font-size"><strong>Idiomes</strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:shortcode -->
            [somit_language_selector]
            <!-- /wp:shortcode -->
          </div>
          <!-- /wp:group -->
        </div>
        <!-- /wp:group -->

        <!-- wp:group {"className":"is-style-show-mobile-tablet","layout":{"type":"flex","orientation":"vertical"}} -->
        <div class="wp-block-group is-style-show-mobile-tablet"><!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"}},"className":"is-style-default","layout":{"type":"default"}} -->
          <div class="wp-block-group is-style-default"><!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|main"}}}},"textColor":"main","fontSize":"medium"} -->
            <p class="has-main-color has-text-color has-link-color has-medium-font-size"><strong>Xarxes socials</strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:list {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"is-style-no-disc"} -->
            <ul class="is-style-no-disc has-base-color has-text-color has-link-color"><!-- wp:list-item -->
              <li><a href="https://www.linkedin.com/">Linkedin</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="https://twitter.com/">Twitter</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="https://mastodon.social">Mastodon</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="https://github.com">Github</a></li>
              <!-- /wp:list-item -->

              <!-- wp:list-item -->
              <li><a href="https://gitlab.com">Gitlab</a></li>
              <!-- /wp:list-item -->
            </ul>
            <!-- /wp:list -->

            <!-- wp:spacer {"height":"1em","width":"0px","style":{"layout":{}}} -->
            <div style="height:1em;width:0px" aria-hidden="true" class="wp-block-spacer"></div>
            <!-- /wp:spacer -->
          </div>
          <!-- /wp:group -->

          <!-- wp:group {"style":{"layout":{"selfStretch":"fixed","flexSize":"50%"},"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"is-style-default","layout":{"type":"default"}} -->
          <div class="wp-block-group is-style-default has-base-color has-text-color has-link-color"><!-- wp:paragraph {"style":{"elements":{"link":{"color":{"text":"var:preset|color|main"}}}},"textColor":"main","fontSize":"medium"} -->
            <p class="has-main-color has-text-color has-link-color has-medium-font-size"><strong>Idiomes</strong></p>
            <!-- /wp:paragraph -->

            <!-- wp:shortcode -->
            [somit_language_selector]
            <!-- /wp:shortcode -->
          </div>
          <!-- /wp:group -->
        </div>
        <!-- /wp:group -->
      </div>
      <!-- /wp:column -->

      <!-- wp:column -->
      <div class="wp-block-column"><!-- wp:shortcode -->
        [contact-form-7 id="ca6c21c" title="Contacte"]
        <!-- /wp:shortcode -->
      </div>
      <!-- /wp:column -->
    </div>
    <!-- /wp:columns -->
  </div>
  <!-- /wp:group -->

  <!-- wp:spacer {"height":"2rem"} -->
  <div style="height:2rem" aria-hidden="true" class="wp-block-spacer"></div>
  <!-- /wp:spacer -->

  <!-- wp:group {"layout":{"type":"constrained"},"fontSize":"x-small"} -->
  <div class="wp-block-group has-x-small-font-size"><!-- wp:group {"className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
    <div class="wp-block-group is-style-show-desktop"><!-- wp:paragraph -->
      <p>Som It Cooperatiu</p>
      <!-- /wp:paragraph -->

      <!-- wp:list {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"is-style-no-disc"} -->
      <ul id="somit-policy-nav" class="is-style-no-disc has-base-color has-text-color has-link-color"><!-- wp:list-item -->
        <li>Privacitat</li>
        <!-- /wp:list-item -->

        <!-- wp:list-item -->
        <li>Avís legal</li>
        <!-- /wp:list-item -->

        <!-- wp:list-item -->
        <li><a href="/cookie-policy-eu/">Cookies</a></li>
        <!-- /wp:list-item -->
      </ul>
      <!-- /wp:list -->
    </div>
    <!-- /wp:group -->

    <!-- wp:group {"className":"is-style-show-mobile-tablet","layout":{"type":"flex","orientation":"vertical","justifyContent":"center"}} -->
    <div class="wp-block-group is-style-show-mobile-tablet"><!-- wp:list {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"is-style-no-disc"} -->
      <ul id="somit-policy-nav" class="is-style-no-disc has-base-color has-text-color has-link-color"><!-- wp:list-item -->
        <li>Privacitat</li>
        <!-- /wp:list-item -->

        <!-- wp:list-item -->
        <li>Avís legal</li>
        <!-- /wp:list-item -->

        <!-- wp:list-item -->
        <li><a href="/cookie-policy-eu/">Cookies</a></li>
        <!-- /wp:list-item -->
      </ul>
      <!-- /wp:list -->

      <!-- wp:paragraph -->
      <p>Som It Cooperatiu</p>
      <!-- /wp:paragraph -->
    </div>
    <!-- /wp:group -->
  </div>
  <!-- /wp:group -->
  <!-- wp:spacer {"height":"1rem"} -->
  <div style="height:1rem" aria-hidden="true" class="wp-block-spacer"></div>
  <!-- /wp:spacer -->
</div>
<!-- /wp:group -->
