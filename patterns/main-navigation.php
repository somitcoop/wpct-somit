<?php

/**
 * Title: Main Navigation
 * Slug: somit-pattern/navigation
 * Categories: somit-pattern
 * Viewport Width: 1500
 */
?>

<!-- wp:navigation-submenu {"label":"Qui som","type":"page","id":172,"url":"#qui-som","kind":"post-type"} -->
<!-- wp:navigation-link {"label":"Missió i valors","url":"#mission","kind":"custom"} /-->

<!-- wp:navigation-link {"label":"Principis","url":"#principles","kind":"custom"} /-->

<!-- wp:navigation-link {"label":"Socis","url":"#partners","kind":"custom"} /-->
<!-- /wp:navigation-submenu -->

<!-- wp:navigation-link {"label":"Serveis","url":"#services","kind":"custom"} /-->

<!-- wp:navigation-link {"label":"Productes","url":"#products","kind":"custom"} /-->

<!-- wp:navigation-link {"label":"Projectes","url":"#projects","kind":"custom"} /-->

<!-- wp:navigation-link {"label":"Equip","url":"#team","kind":"custom"} /-->

<!-- wp:navigation-link {"label":"Contacte","url":"#contact","kind":"custom"} /-->
