<?php

/**
 * Title: SomIT Header
 * Slug: somit-pattern/header
 * Categories: somit-pattern
 * Viewport Width: 1500
 */

?>

<!-- wp:group {"align":"full","backgroundColor":"brand","className":"is-style-horizontal-padded","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull is-style-horizontal-padded has-brand-background-color has-background"><!-- wp:group {"style":{"spacing":{"padding":{"top":"var(\u002d\u002dwp\u002d\u002dcustom\u002d\u002dspacing\u002d\u002dsm)","bottom":"var(\u002d\u002dwp\u002d\u002dcustom\u002d\u002dspacing\u002d\u002dsm)"},"margin":{"top":"0px","bottom":"0"}}},"className":"is-style-horizontal-padded","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"space-between"}} -->
    <div class="wp-block-group is-style-horizontal-padded" style="margin-top:0px;margin-bottom:0;padding-top:var(--wp--custom--spacing--sm);padding-bottom:var(--wp--custom--spacing--sm)"><!-- wp:image {"id":247,"width":"163px","height":"40px","scale":"contain","sizeSlug":"full","linkDestination":"custom","style":{"layout":{"selfStretch":"fixed","flexSize":"163px"}},"className":"is-style-default"} -->
        <figure class="wp-block-image size-full is-resized is-style-default"><a href="/"><img src="https://somit.coop/wp-content/uploads/2024/01/Capa_1.png" alt="" class="wp-image-247" style="object-fit:contain;width:163px;height:40px" /></a></figure>
        <!-- /wp:image -->

        <!-- wp:navigation {"ref":8,"textColor":"typography","overlayMenu":"never","overlayBackgroundColor":"main","overlayTextColor":"white","className":"is-style-show-desktop","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"right"},"style":{"layout":{"selfStretch":"fit","flexSize":null},"typography":{"textTransform":"none"}}} /-->

        <!-- wp:navigation {"ref":218,"textColor":"typography","backgroundColor":"brand","overlayMenu":"always","overlayBackgroundColor":"brand","overlayTextColor":"typography","className":"is-style-show-mobile-tablet","layout":{"type":"flex","flexWrap":"nowrap","justifyContent":"stretch","orientation":"vertical"},"style":{"layout":{"selfStretch":"fit","flexSize":null},"typography":{"textTransform":"none"}},"fontSize":"large"} /-->

        <!-- wp:group {"style":{"elements":{"link":{"color":{"text":"var:preset|color|base"}}}},"textColor":"base","className":"is-style-show-desktop","layout":{"type":"default"}} -->
        <div class="wp-block-group is-style-show-desktop has-base-color has-text-color has-link-color"><!-- wp:shortcode -->
            [somit_main_language_selector]
            <!-- /wp:shortcode -->
        </div>
        <!-- /wp:group -->

        <!-- wp:paragraph -->
        <p><a href="https://wordpress.com/home/somit.coop"></a></p>
        <!-- /wp:paragraph -->

        <!-- wp:image {"linkDestination":"custom"} -->
        <figure class="wp-block-image"><a href="https://wordpress.com/home/somit.coop"><img src="https://somit.coop/wp-content/uploads/2024/01/favicon4_somit.png" alt="Site Icon" /></a></figure>
        <!-- /wp:image -->
    </div>
    <!-- /wp:group -->
</div>
<!-- /wp:group -->
