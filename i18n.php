<?php

/*
   * String translations
   */
global $SOMIT_I18N;
$SOMIT_I18N = [
    'ca' => [
        'value="Submit"' => 'value="Enviar"',
    ],
    'es' => [
        // Navigation
        '<span class="wp-block-navigation-item__label">Qui som<\/span>' => '<span class="wp-block-navigation-item__label">Quenes somos</span>',
        '<a class="wp-block-navigation-item__content" href="#qui-som">Qui som<\/a>' => '<a class="wp-block-navigation-item__content" href="#qui-som">Quienes somos</a>',
        'aria-label="Qui som submenu"' => 'aria-label="Quienes somos submenu"',
        '<span class="wp-block-navigation-item__label">Missió i valors<\/span>' => '<span class="wp-block-navigation-item__label">Missión y valores</span>',
        '<span class="wp-block-navigation-item__label">Principis<\/span>' => '<span class="wp-block-navigation-item__label">Principios</span>',
        '<span class="wp-block-navigation-item__label">Socis<\/span>' => '<span class="wp-block-navigation-item__label">Socios</span>',
        '<span class="wp-block-navigation-item__label">Serveis<\/span>' => '<span class="wp-block-navigation-item__label">Servicios</span>',
        '<span class="wp-block-navigation-item__label">Productes<\/span>' => '<span class="wp-block-navigation-item__label">Productos</span>',
        '<span class="wp-block-navigation-item__label">Projectes<\/span>' => '<span class="wp-block-navigation-item__label">Proyectos</span>',
        '<span class="wp-block-navigation-item__label">Equip<\/span>' => '<span class="wp-block-navigation-item__label">Equipo</span>',
        '<span class="wp-block-navigation-item__label">Contacte<\/span>' => '<span class="wp-block-navigation-item__label">Contacto</span>',
        // Footer
        '<strong>Dades de l&#8217;entitat<\/strong>' => '<strong>Datos de la entidad</strong>',
        '<strong>Enllaços<\/strong>' => '<strong>Enlaces</strong>',
        '<a href="#services">Serveis<\/a>' => '<a href="#services">Servicios</a>',
        '<a href="#products">Productes<\/a>' => '<a href="#products">Productos</a>',
        '<a href="#projects">Projectes<\/a>' => '<a href="#projects">Proyectos</a>',
        '<a href="#team">Equip<\/a>' => '<a href="#team">Equipo</a>',
        '<strong>Xarxes socials<\/strong>' => '<strong>Redes sociales</strong>',
        '<strong>Idiomes<\/strong>' => '<strong>Idiomas</strong>',
        // Contact form
        'placeholder="Nom"' => 'placeholder="Nombre"',
        'placeholder="Correu electrònic"' => 'placeholder="Correo electrónico"',
        'placeholder="Assumpte"' => 'placeholder="Asunto"',
        'placeholder="Missatge"' => 'placeholder="Mensaje"',
        'value="Submit"' => 'value="Enviar"',
        // Legal links
        'Em dono per assabentat\/da de la política de privacitat, i l’accepto.' => 'Me doy por informado/a de la política de privacidad, y la acepto',
        'INFORMACIÓ DE PROTECCIÓ DE DADES. Responsable\: Som It SCCL. Finalitats\: Enviar-te comunicacions comercials sobre l’estat d’aquest projecte per mitjans electrònics. Drets: Pots retirar el teu consentiment en qualsevol moment, a més d’accedir, rectificar, suprimir les teves dades i altres al correu: somenergia@delegado-datos.com. Informació addicional: Política de Privacitat.' => 'INFORMACIÓN DE PROTECCIÓN DE DATOS. Responsable: Som It SCCL. Finalidades: Enviarte comunicaciones comerciales sobre el estado del proyecto por medios electrnóicos. Derechos: Puedes retirar tu consentimiento en cualquier momento, además de acceder, rectificar, suprimir tus datos y otros a través del correo: somenergia@delegado-datos.com. Información adicional: Política de Privacidad.',
        '<li><a href="\/avis-legal\/">Avís legal<\/a><\/li>' => '<li><a href="/es/aviso-legal/">Aviso legal</a></li>',
        '<li><a href="\/politica-de-privacitat\/">Privacitat<\/a><\/li>' => '<li><a href="/politica-de-privacidad/">Privacidad</a></li>',
    ],
    'en' => [
        // Navigation
        '<span class="wp-block-navigation-item__label">Qui som<\/span>' => '<span class="wp-block-navigation-item__label">About us</span>',
        '<a class="wp-block-navigation-item__content" href="#qui-som">Qui som<\/a>' => '<a class="wp-block-navigation-item__content" href="#qui-som">About us</a>',
        'aria-label="Qui som submenu"' => 'aria-label="About us submenu"',
        '<span class="wp-block-navigation-item__label">Missió i valors<\/span>' => '<span class="wp-block-navigation-item__label">Mission and values</span>',
        '<span class="wp-block-navigation-item__label">Principis<\/span>' => '<span class="wp-block-navigation-item__label">Principles</span>',
        '<span class="wp-block-navigation-item__label">Socis<\/span>' => '<span class="wp-block-navigation-item__label">Partners</span>',
        '<span class="wp-block-navigation-item__label">Serveis<\/span>' => '<span class="wp-block-navigation-item__label">Services</span>',
        '<span class="wp-block-navigation-item__label">Productes<\/span>' => '<span class="wp-block-navigation-item__label">Products</span>',
        '<span class="wp-block-navigation-item__label">Projectes<\/span>' => '<span class="wp-block-navigation-item__label">Projects</span>',
        '<span class="wp-block-navigation-item__label">Equip<\/span>' => '<span class="wp-block-navigation-item__label">Team</span>',
        '<span class="wp-block-navigation-item__label">Contacte<\/span>' => '<span class="wp-block-navigation-item__label">Contact</span>',
        // Footer
        '<strong>Dades de l&#8217;entitat<\/strong>' => '<strong>Entity information</strong>',
        '<strong>Enllaços<\/strong>' => '<strong>Links</strong>',
        '<a href="#services">Serveis<\/a>' => '<a href="#services">Services</a>',
        '<a href="#products">Productes<\/a>' => '<a href="#products">Products</a>',
        '<a href="#projects">Projectes<\/a>' => '<a href="#projects">Projects</a>',
        '<a href="#team">Equip<\/a>' => '<a href="#team">Team</a>',
        '<strong>Xarxes socials<\/strong>' => '<strong>Social networks</strong>',
        '<strong>Idiomes<\/strong>' => '<strong>Languages</strong>',
        // Contact form
        'placeholder="Nom"' => 'placeholder="Name"',
        'placeholder="Correu electrònic"' => 'placeholder="Email"',
        'placeholder="Assumpte"' => 'placeholder="Subject"',
        'placeholder="Missatge"' => 'placeholder="Message"',
        'Em dono per assabentat\/da de la política de privacitat, i l’accepto.' => 'I acknowledge that I have read the privacy policy and accept it.',
        'INFORMACIÓ DE PROTECCIÓ DE DADES. Responsable\: Som It SCCL. Finalitats\: Enviar-te comunicacions comercials sobre l’estat d’aquest projecte per mitjans electrònics. Drets: Pots retirar el teu consentiment en qualsevol moment, a més d’accedir, rectificar, suprimir les teves dades i altres al correu: somenergia@delegado-datos.com. Informació addicional: Política de Privacitat.' => 'DATA PROTECTION INFORMATION. Responsible: Som It SCCL. Purposes: To send you commercial communications about the status of the project by electronic means. Rights: You can withdraw your consent at any time, as well as access, rectify, delete your data and others through the mail: somenergia@delegado-datos.com. Additional information: Privacy Policy.',
        // Legal links
        '<li><a href="\/avis-legal\/">Avís legal<\/a><\/li>' => '<li><a href="/en/legal-notice/">Legal notice</a></li>',
        '<li><a href="\/politica-de-privacitat\/">Privacitat<\/a><\/li>' => '<li><a href="/privacy-policy/">Privacy</a></li>',
    ]
];
