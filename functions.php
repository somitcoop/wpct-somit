<?php

// require_once 'i18n.php';

/**
 * Enqueue Child theme assets.
 */
add_action('wp_enqueue_scripts', 'somit_enqueue_scripts', 11);
function somit_enqueue_scripts()
{
    $theme = wp_get_theme();
    $parent = $theme->parent();

    wp_enqueue_style(
        $parent->get_stylesheet(),
        $parent->get_stylesheet_directory_uri() . '/style.css',
        [],
        $parent->get('Version')
    );

    wp_enqueue_style(
        $theme->get_stylesheet(),
        $theme->get_stylesheet_directory_uri() . '/style.css',
        [$parent->get_stylesheet()],
        $theme->get('Version')
    );

    wp_enqueue_script('wp-coop-theme-child', $theme->get_stylesheet_directory_uri() . '/js/app.js', ['wp-coop-theme'], $theme->get('Version'));
}

add_filter('wpct_dequeue_fonts', '__return_true');

/**
 * Register block pattern categories
 */
add_action('init', 'somit_register_block_pattern_categories');
function somit_register_block_pattern_categories()
{
    register_block_pattern_category(
        'somit-pages',
        ['label' => __('SomIT Pages')]
    );

    register_block_pattern_category(
        'somit-pattern',
        ['label' => __('SomIT Pattern')]
    );
}

/**
 * Add support to svg media
 */
add_filter('upload_mimes', 'add_file_types_to_uploads');
function add_file_types_to_uploads($file_types)
{
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes);
    return $file_types;
}

add_action('after_setup_theme', 'somit_add_theme_support');
function somit_add_theme_support()
{
    add_theme_support('editor-styles');
    add_editor_style('./css/editor-style.css');
}

add_shortcode('somit_main_language_selector', function () {
    ob_start();
    do_action('wpml_add_language_selector');
    return ob_get_clean();
});

add_shortcode('somit_footer_language_selector', function () {
    ob_start();
    do_action('wpml_footer_language_selector');
    return ob_get_clean();
});
