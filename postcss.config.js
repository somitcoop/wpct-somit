module.exports = {
  "plugins": {
    "postcss-import": {},
    "postcss-nested-ancestors": {},
    "postcss-nested": {},
    "postcss-url": {
      "url": "inline",
    },
    "tailwindcss": {},
    "cssnano": {
      "preset": "default",
    },
  },
};
