const tailpress = require("@jeffreyvr/tailwindcss-tailpress");

module.exports = {
  content: ["./patterns/*.php", "./parts/*.html", "./templates/*.html"],
  theme: {
    extend: {},
  },
  plugins: [tailpress.tailwind],
};
