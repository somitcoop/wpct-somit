document.addEventListener("DOMContentLoaded", () => {
  function setCurrentLink() {
    const currentSection = window.location.hash.replace(/^\/?#/, "");
    for (const navItem of document.querySelectorAll(
      ".wp-block-navigation-item",
    )) {
      if (navItem.classList.contains("has-child")) {
        navItem.classList.remove("current");
      }
      const anchor = navItem.querySelector("a");
      const anchorHash = anchor.href.replace(/^.+\/?#/, "");
      const isSubItem = navItem.parentElement.classList.contains(
        "wp-block-navigation-submenu",
      );
      if (currentSection === anchorHash) {
        navItem.classList.add("current");
        if (isSubItem) {
          navItem.parentElement.parentElement.classList.add("current");
        }
      } else {
        navItem.classList.remove("current");
      }
    }
  }

  window.addEventListener("popstate", setCurrentLink);
  setCurrentLink();

  const siteHeader = document.querySelector(".site-header");
  const sections = Array.from(
    document.querySelectorAll(".entry-content section.wp-block-group"),
  )
    .concat(document.querySelector("#contact"))
    .filter((el) => el.offsetParent);

  sections.forEach((section) => {
    const id = section.id;
    section.removeAttribute("id");
    section.setAttribute("data-id", id);
    const scrollAnchor = document.createElement("span");
    scrollAnchor.classList.add("top");
    scrollAnchor.id = id;
    section.insertBefore(scrollAnchor, section.children[0]);
  });
  window.addEventListener("scroll", () => {
    if (document.documentElement.scrollTop > 50) {
      siteHeader.classList.add("drop-shadow");
    } else {
      siteHeader.classList.remove("drop-shadow");
    }

    for (const section of sections) {
      const { top } = section.getBoundingClientRect();
      if (Math.abs(top) < 100) {
        const hash = `#${section.dataset.id}`;
        if (hash !== window.location.hash) {
          window.history.replaceState(
            { from: window.location.hash, to: hash },
            null,
            `${window.location.pathname}${window.location.search}${hash}`,
          );
          window.dispatchEvent(new Event("popstate"));
        }
        break;
      }
    }
  });

  const modal = document.querySelector(".wp-block-navigation__responsive-container");
  if (!modal) return;
  const modalLinks = modal.querySelectorAll(".wp-block-navigation-item a");
  const modalOpenner = document.querySelector(
    ".wp-block-navigation__responsive-container-open",
  );
  const modalCloser = document.querySelector(
    ".wp-block-navigation__responsive-container-close",
  );
  modalOpenner.addEventListener("click", () => {
    modal.classList.add("is-menu-open");
    modal.classList.add("has-modal-open");
  });
  modalCloser.addEventListener("click", () => {
    modal.classList.remove("is-menu-open");
    modal.classList.remove("has-modal-open");
  });

  for (let link of modalLinks) {
    link.addEventListener("click", () => {
      modal.classList.remove("is-menu-open");
      modal.classList.remove("has-modal-open");
    });
  }
});
